BullsAndCows

How to play
1. Computer will choose 4-digit random number whose digits are not duplicated
   For examples, case like 1111 is not allowed
2. Input 4-dight number whose digits are not duplicated
3. Location of number that you input is as same as random number: Strike(S),
   Location is different but number is same: Ball(B),
   Every number is not correct: Out(O),
   Every number is correct: Home-Run

   example:
   If random number is 1234 and you're number is 4215,
   4, 1 is in the random number but location is different, so 2Ball,
   2 is in random number and location is also same, so 1Strike,
   5 is not in random number, so 1Out
   
   Output would be like this:
   2B 1S 1O
4. If you got Home-Run, you won but if you don't, go to number 2

플레이하는 방법
1. 컴퓨터가 4자리 서로 중복되지 않는 랜덤 숫자 하나를 뽑습니다
   예를 들어, 1111과 같은 경우는 허용되지 않습니다
2. 중복되지 않는 4자리 숫자를 입력합니다
3. 입력한 수의 자릿수와 랜덤 숫자의 위치가 같으면 스트라이크(S),
   위치는 다르지만 숫자는 맞을 경우에는 볼(B),
   맞지 않을 경우에는 아웃(O),
   모두 일치하면 홈런입니다

   예)
   랜덤 숫자가 1234이고, 입력한 숫자가 4215이면
   4, 1은 숫자는 같지만 위치가 다르므로 2볼,
   2는 위치와 숫자 모두 같으므로 1스트라이크,
   5는 해당되는 숫자가 없으므로 1아웃입니다
   
   출력은
   2B 1S 1O
   와 같이 됩니다
5. 홈런인 경우에는 승리하고, 아닌 경우에는 2로 돌아갑니다
