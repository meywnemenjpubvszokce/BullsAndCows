#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

int main() {
    char randomNumber[4];
    char userNumber[4];
    unsigned int strike, ball, out;
    int i, j;

    srand(time(NULL));

    for (i = 0; i < 4; i++) {
        randomNumber[i] = rand() % 9 + '0';
        for (j = 0; j < i; j++) {
            if (randomNumber[j] == randomNumber[i]) {
                i--;
            }
        }
    }

    for (i = 0; i < 4; i++) {
        printf("%c", randomNumber[i]);
    }
    putchar('\n');

    do {
        strike = 0;
        ball   = 0;
        out    = 0;

        bool isDuplicated = false;
        
        scanf("%s", userNumber);

        for (i = 0; i < 4 && !isDuplicated; i++) {
            for (j = i + 1; j < 4; j++) {
                if (userNumber[i] == userNumber[j]) {
                    printf("Duplicated input: %c\n", userNumber[i]);
                    
                    isDuplicated = true;
                    
                    break;
                }
            }
        }

        if (isDuplicated) {
            continue;
        }

        for (i = 0; i < 4; i++) {
            for (j = 0; j < 4; j++) {
                if (randomNumber[i] == userNumber[j]) {
                    if (i == j) {
                        strike++;
                    } else {
                        ball++;
                    }
                }
            }
        }

        out = 4 - strike - ball;
        printf("%dS %dB %dO\n", strike, ball, out);
    } while (strike != 4);

    printf("Home-Run\n");

    return 0;
}
